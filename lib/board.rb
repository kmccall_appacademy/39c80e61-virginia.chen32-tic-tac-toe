class Board
  def initialize(grid = [[nil, nil, nil], [nil, nil, nil],
    [nil, nil, nil]])
    #@grid = [[0,0],[0,1],[0,2],[1,0],[1,1],[1,2],[2,0],[2,1],[2,2]]
    @grid  = grid
  end

  attr_accessor :grid

  def place_mark(pos,sym)
    if empty?(pos)==false
      raise Exception.new("This position is already taken")
    end
    @grid[pos[0]][pos[1]]=sym
  end

  def empty?(pos)
    return true if grid[pos[0]][pos[1]] == nil
  end

  def winner
    if winner?(:X,grid)
      return :X
    elsif winner?(:O,grid)
      return :O
    else return nil
    end
  end

  def winner?(sym,board)
    vertical1 = [board[0][0],board[1][0],board[2][0]]
    vertical2 = [board[0][1],board[1][1],board[2][1]]
    vertical3 = [board[0][2],board[1][2],board[2][2]]
    diagonal1 = [board[0][0],board[1][1],board[2][2]]
    diagonal2 = [board[2][0],board[1][1],board[0][2]]
    row1=board[0]
    row2=board[1]
    row3=board[2]
    possible_wins = [vertical1,vertical2,vertical3,diagonal1,
      diagonal2,row1,row2,row3]
    possible_wins.each do |option|
      return true if option.count(sym)==3
    end
    return false
  end

  def over?
    if grid.flatten.include?(nil)==false && winner == nil
      return true
    elsif winner != nil
      return true
    elsif grid.include?(nil)
      return false
    end
  end

end
