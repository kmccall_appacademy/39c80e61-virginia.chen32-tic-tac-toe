require_relative 'board'

class ComputerPlayer

  def initialize(name="Bonzo")
    @mark = :O
    @name = name
  end

  attr_accessor :mark, :name, :board, :grid

  def display(board)
    print @board = board
  end

  def board
    @grid = @board
  end

  def get_move
    if get_winning_move(@mark,@board.grid) != nil
      get_winning_move(@mark,@board.grid)
    else get_random_move(@board.grid)
    end
  end

  def get_winning_move(sym,grid)
    winning_move = nil
    new_grid = @board.grid
    @board.grid.each_with_index do |row,idx|
      row.each_with_index do |pos,index|
        if pos == nil
          new_grid[idx][index]=sym
          if @board.winner?(sym,new_grid)
            winning_move = [idx,index]
          end
          new_grid[idx][index]=nil
        end
      end
    end
    return winning_move
  end

  def get_random_move(board)
    x = rand(2)
    y = rand(2)
    spot = board[x][y]
    until spot == nil
      x = rand(2)
      y = rand(2)
      spot = board[x][y]
    end
    [x,y]
  end

end
