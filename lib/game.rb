require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  def initialize(player1,player2)
    @player1=player1
    @player2=player2
    @board = Board.new()
    @current_player = player1
  end

  attr_accessor :player1,:player2,:board,:current_player

  def play_turn
    board.place_mark(current_player.get_move,current_player.mark)
    if current_player.instance_of? HumanPlayer
      @current_player.display(@board)
    end
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else @current_player = @player1
    end
  end
end

if $PROGRAM_NAME == __FILE__
  Game.new(HumanPlayer.new("Virginia"),ComputerPlayer.new).play
end
