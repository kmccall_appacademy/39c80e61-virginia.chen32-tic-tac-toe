require_relative 'board'

class HumanPlayer
  def initialize(name)
    @mark = :X
    @name=name
  end

  attr_accessor :mark, :name, :board

  def display(board)
    puts board.grid
  end

  def get_move
    puts "where should I move?"
    move = gets.chomp
    move_loc = [move[0].to_i,move[-1].to_i]
    move_loc
  end
end
